var app = angular.module('mech', ['ngRoute', 'ui.bootstrap', 'main-controller',
	'login-controller', 'register-controller', 'auth-service', 'session-service'
])

// ===================================
//  			CONSTANTS
// ===================================

.constant('AUTH_EVENTS',
{
	loginSuccess: 'auth-login-success',
	loginFailed: 'auth-login-failed',
	logoutSuccess: 'auth-logout-success',
	sessionTimeout: 'auth-session-timeout',
	notAuthenticated: 'auth-not-authenticated',
	notAuthorized: 'auth-not-authorized'
})

.constant('USER_ROLES',
{
	all: '*',
	admin: 'admin',
	user: 'user',
	guest: 'guest'
})

.value('API_URL', 'https://localhost:1337')

// ===================================
//  			CONFIG
// ===================================
.config(function($httpProvider)
{
	$httpProvider.interceptors.push([
		'$injector',
		function($injector)
		{
			return $injector.get('AuthInterceptor');
		}
	]);
})

.factory('AuthInterceptor', function($rootScope, $q,
	AUTH_EVENTS)
{
	return {
		responseError: function(response)
		{
			$rootScope.$broadcast(
			{
				401: AUTH_EVENTS.notAuthenticated,
				403: AUTH_EVENTS.notAuthorized,
				419: AUTH_EVENTS.sessionTimeout,
				440: AUTH_EVENTS.sessionTimeout
			}[response.status], response);
			return $q.reject(response);
		}
	};
})

// ===================================
//  			  RUN
// ===================================

app.run(function($rootScope, $location, Auth, AUTH_EVENTS, $http)
{

	$rootScope.$on('$routeChangeStart', function()
	{
		if (!Auth.isAuthenticated() && $location.path() !== "/login" && $location.path() !==
			"/register")
		{
			//login successful, now get a jwt
			console.log('Prevented page change');
			event.preventDefault();
			$location.path('/login');
		}
	})
});