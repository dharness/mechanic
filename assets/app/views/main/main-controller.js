angular.module('main-controller', [])


.controller('MainController', ['$scope', '$location', '$http', 'API_URL',
	'Session',

	function($scope, $location, $http, API_URL, Session)
	{
		$scope.user;
		$scope.problems = ['lemmon'];

		/*******************************
				   Load Profile
		********************************/
		$http.get(API_URL + '/user/profile',
		{
			headers:
			{
				'access_token': Session.token
			}
		}).then(function(data)
		{
			$scope.user = data.data;
		});

		/*******************************
				   Load Problems
		********************************/
		$http.get(API_URL + '/problem',
		{
			headers:
			{
				'access_token': Session.token
			}
		}).then(function(data)
		{
			$scope.problems = data.data;
		});

		$scope.logout = function()
		{
			Session = null;
			$location.path('/login')
		}
	}
]);