angular.module('register-controller', [])


.controller('RegisterController', ['$rootScope', '$scope', '$http', '$location',
	'Auth', 'AUTH_EVENTS',

	function($rootScope, $scope, $http, $location, Auth, AUTH_EVENTS)
	{
		$scope.tmpUser = {};

		$scope.register = function(tmpUser)
		{
			Auth.register(tmpUser).then(function()
			{
				console.log('tmpUser');
				$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
				$location.path('/')
			}, function()
			{
				$rootScope.$broadcast(AUTH_EVENTS.loginFailed);

				toastr.options = {
					"closeButton": false,
					"debug": false,
					"newestOnTop": false,
					"progressBar": false,
					"positionClass": "toast-bottom-right",
					"preventDuplicates": false,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "5000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				}
				toastr["error"]("Invalid Registration!")

			});
		};
	}
]);