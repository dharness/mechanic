angular.module('login-controller', [])


.controller('LoginController', ['$rootScope', '$scope', '$http', '$location',
	'Auth', 'AUTH_EVENTS',

	function($rootScope, $scope, $http, $location, Auth, AUTH_EVENTS)
	{
		$scope.tmpUser = {};


		////////////////////////TEMPORARY DEV BIPASS////////////////////////
		Auth.login(
		{
			email: 'dylan@dylan.com',
			password: 'bluecakes'
		}).then(function()
		{
			$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
			$location.path('/')
		});
		/////////////////////////////////////////////////////////////////////

		$scope.login = function(tmpUser)
		{
			Auth.login(tmpUser).then(function()
			{
				$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
				$location.path('/')
			}, function()
			{
				console.log('here');
				$rootScope.$broadcast(AUTH_EVENTS.loginFailed);

				toastr.options = {
					"closeButton": false,
					"debug": false,
					"newestOnTop": false,
					"progressBar": false,
					"positionClass": "toast-bottom-right",
					"preventDuplicates": false,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "5000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
				}
				toastr["error"]("Invalid Login!")

			});
		};

		$scope.flash = function() {

		}


	}
]);