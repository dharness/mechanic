angular.module('auth-service', [])

.factory('Auth', function($rootScope, $http, $q, Session, AUTH_EVENTS, API_URL)
{
	var user;

	return {
		login: function(options)
		{
			return $q(function(resolve, reject)
			{
				// perform basic login
				$http.post(API_URL + '/auth/login', options)
					.success(function(data)
					{
						var user = {};
						Session.create(data.token, options.email)
						resolve(user);
					})
					.error(function(err)
					{
						reject(err)
					})
			})

		},
		register: function(options)
		{
			console.log(options);
			return $q(function(resolve, reject)
			{
				// perform basic login
				$http.post(API_URL + '/auth/register', options)
					.success(function(data)
					{
						console.log('success');
						var user = {};
						Session.create(data.token, options.email)
						resolve(user);
					})
					.error(function(err)
					{
						console.log(err);
						reject(err)
					})
			})
		},
		isAuthenticated: function()
		{
			return !!Session.email;
		}
	}

});