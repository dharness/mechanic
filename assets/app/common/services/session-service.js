angular.module('session-service', [])

.service('Session', function()
{

	this.create = function(token, email)
	{
		this.token = token;
		this.email = email;
	};

	this.destroy = function()
	{
		this.token = null;
		this.email = null;
	};

});