app.config(function($routeProvider)
{
	$routeProvider

	.when('/',
	{
		templateUrl: 'app/views/main/main-view.html',
		controller: 'MainController'
	})

	.when('/login',
	{
		templateUrl: 'app/views/login/login-view.html',
		controller: 'LoginController'
	})

	.when('/register',
	{
		templateUrl: 'app/views/register/register-view.html',
		controller: 'RegisterController'
	})


});