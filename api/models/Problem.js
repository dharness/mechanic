/**
 * Problem.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

	attributes:
	{
		level:
		{
			type: 'INT',
			required: true,
			unique: true
		},
		statement:
		{
			type: 'STRING',
			required: true
		},
		name:
		{
			type: 'STRING',
			required: true
		}

	}
};