/**
 * User
 *
 * @module      :: Model
 * @description :: This is the base user model
 * @docs        :: http://waterlock.ninja/documentation
 */

module.exports = {

	schema: true,


	attributes:
	{
		name:
		{
			type: 'STRING',
			required: true
		},
		email:
		{
			type: 'EMAIL',
			unique: true,
			required: true
		},
		encryptedPassword:
		{
			type: 'STRING',
		},
		solutions:
		{
			type: 'ARRAY'
		},
		toJSON: function()
		{
			var obj = this.toObject();
			delete obj.encryptedPassword;
			return obj;
		}
	},
	beforeCreate: function(values, next)
	{
		console.log(values);
		if (!values.password || values.password != values.confirmPassword)
		{
			return next(
			{
				err: 'Password combination invalid'
			})
		}
		else
		{
			require('bcrypt').hash(values.password, 8, function(err, hash)
			{
				values.encryptedPassword = hash;
				values.solutions = [];
				next();
			});
		}
	}
};