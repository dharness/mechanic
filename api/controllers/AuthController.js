/**
 * AuthController.js
 *
 * @module      :: Controller
 * @description :: Provides authentication and registration functionality
 *
 */
module.exports = {
	login: function(req, res)
	{
		var email = req.param('email');
		if (email)
		{
			User.findOne(
			{
				where:
				{
					email: email
				}
			}, function(err, user)
			{
				if (!user)
				{
					return res.badRequest(
					{
						err: 'No user found'
					})
				}
				else
				{
					require('bcrypt').compare(req.param('password'), user.encryptedPassword,
						function(err, isMatch)
						{
							if (!isMatch)
							{
								return res.badRequest(
								{
									err: 'Incorrect password'
								})
							}
							res.json(
							{
								token: tokenAuth.issueToken(email),
								user: user
							})
						});
				}
			});
		}
		else
		{
			res.badRequest('no email')
		}

	},

	register: function(req, res)
	{
		var email = req.param('email');
		console.log(req.params.all())
		User.create(req.params.all(), function(err, user)
		{
			if (err)
			{
				return res.badRequest(err);
			}
			res.json(
			{
				token: tokenAuth.issueToken(email),
				user: user
			})
		})
	}
}