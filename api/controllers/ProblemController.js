/**
 * ProblemController.js
 *
 * @module      :: Controller
 * @description :: Provides the interface for retrieving or changing problem sets
 *
 * @docs        :: http://waterlock.ninja/documentation
 */

module.exports = {

	find: function(req, res)
	{

		var id = req.params.all().id;
		if (id)
		{
			Problem.findOne(
			{
				where:
				{
					level: parseInt(id)
				}
			}, function(err, problem)
			{
				if (!problem)
				{
					return res.json('Problem not found')
				}
				res.json(problem)
			})
		}
		else
		{
			Problem.find(function(err, data)
			{
				return res.json(data)
			})
		}

	}

}