/**
 * UserController.js
 *
 * @module      :: Controller
 * @description :: Provides the base user
 *                 actions used to make waterlock work.
 *
 * @docs        :: http://waterlock.ninja/documentation
 */

var jwt = require('jsonwebtoken')

module.exports = {
	profile: function(req, res)
	{
		var token = req.headers['access_token'];
		var decodedEmail = jwt.decode(token);
		User.findOne(
		{
			where:
			{
				email: decodedEmail
			}
		}, function(err, user)
		{
			res.json(user)
		})
	}
};