/**
 * NicksbrainController.js
 *
 * @module      :: Controller
 * @description :: Provides the interface for asserting solutions to problems
 *
 * @docs        :: http://waterlock.ninja/documentation
 */

module.exports = {

	solve: function(req, res)
	{
		var id = req.params.all().id;
		var solution = req.params.all().solution;

		if (!id || !solution)
		{
			return res.badRequest('Please provide a solution');
		}

		Problem.findOne(
		{
			where:
			{
				level: parseInt(id)
			}
		}, function(err, problem)
		{
			if (err)
			{
				// res.badRequest('no problem with that id')
			}

			res.json(
			{
				problem: problem,
				solution: solution
			})
		})
	}

}