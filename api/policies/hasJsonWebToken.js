var jwt = require('jsonwebtoken')

module.exports = function(req, res, next)
{
	jwt.verify(req.headers['access_token'], process.env.TOKEN_SECRET ||
		"our biggest secret", function(err, decoded)
		{
			if (err)
			{
				return res.send(err)
			}
			else if (decoded)
			{
				next();
			}
			else
			{
				return res.send(403)
			}
		});
};